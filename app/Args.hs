-- app/Args.hs: application configuration from command line
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module Args
  ( root
  ) where


import           Control.Exception              ( IOException
                                                , try
                                                )

import           Data.Functor                   ( (<&>) )

import           Options.Applicative

import           System.Directory               ( getCurrentDirectory )
import           System.FilePath                ( takeDirectory )


newtype Args = Args { getRoot :: FilePath }


parseCli_ :: Parser Args
parseCli_ = Args <$> strOption
  (long "root" <> short 'r' <> metavar "ROOT" <> value "" <> help
    "Starting point to scan for Git repositories"
  )


-- Initialize `Args` via command line argument(s)
fromCli :: IO Args
fromCli = execParser $ info
  (parseCli_ <**> helper)
  (header
    "Git sub-command to recursively locate .git directories and run gc on them."
  )


-- Extract root from the configuration or fallback to default
root :: IO FilePath
root = fromCli >>= homeIfEmpty . getRoot
 where
  homeIfEmpty []   = defaultRoot
  homeIfEmpty path = pure $ takeDirectory path


-- Return home directory of the current user or `/dev/null` if there are any
-- errors.
defaultRoot :: IO FilePath
defaultRoot =
  (try getCurrentDirectory :: IO (Either IOException FilePath))
    <&> pathOrDevNull
 where
  pathOrDevNull (Right path) = path
  pathOrDevNull _            = "/dev/null"
