-- app/Main.hs: main entry point for the git-rgc executable.
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module Main where


import           Brick.BChan                    ( newBChan
                                                , writeBChan
                                                )
import           Brick.Main                     ( customMain )

import           Control.Concurrent             ( forkIO )

import           Graphics.Vty                   ( mkVty )

import           App                            ( app )
import qualified Args
import           Model


main :: IO ()
main = do
  let buildVty = mkVty mempty

  initialVty <- buildVty
  root       <- Args.root
  msgChannel <- newBChan 10
  _          <- forkIO . writeBChan msgChannel . RootSelected $ root

  _          <- customMain initialVty
                           buildVty
                           (Just msgChannel)
                           app
                           (Lookup msgChannel root)

  pure ()
