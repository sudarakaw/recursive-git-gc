-- app/Model.hs: Application state
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module Model
  ( Model(..)
  , Msg(..)
  , ResourceName(..)
  , isComplete
  , getRepos
  , withGitRepo
  , withRepoMap
  ) where


import           Brick.BChan                    ( BChan )

import           Control.Concurrent.MVar

import qualified Data.HashMap.Strict           as Map

import           GitRepo                        ( GitRepo(..)
                                                , RepoMap
                                                , RepoStatus(..)
                                                )


-- MODEL


data Model = Lookup   { getChannel :: BChan Msg, _root :: FilePath }
           | Process  { getChannel :: BChan Msg, _repos :: RepoMap }
           | Complete { getChannel :: BChan Msg, _repos :: RepoMap }

data Msg = RootSelected FilePath
         | RawRepoFound (MVar GitRepo)
         | GotRepoSize GitRepo
         | GCComplete GitRepo

data ResourceName = ViewportBody
  deriving (Show, Ord, Eq)


-- ACCESSORS


getRepos :: Model -> RepoMap
getRepos (Process  _ rs) = rs
getRepos (Complete _ rs) = rs
getRepos _               = Map.empty


-- TRANSFORMERS


withRepoMap :: RepoMap -> Model -> Model
withRepoMap rs (              Lookup   channel _) = Process channel rs
withRepoMap rs processModel@( Process  _       _) = processModel { _repos = rs }
withRepoMap _  completeModel@(Complete _       _) = completeModel

withGitRepo :: GitRepo -> Model -> Model
withGitRepo repo@(GitRepo path _ _ _) (Lookup channel _) =
  Process channel (Map.singleton path repo)
withGitRepo repo@(GitRepo path _ _ _) processModel@(Process _ repos) =
  processModel { _repos = Map.adjust (const repo) path repos }
withGitRepo _ completeModel@(Complete _ _) = completeModel

isComplete :: Model -> Model
isComplete completeModel@(Complete _ _) = completeModel
isComplete model                        = if noPendingRepos
  then Complete (getChannel model) repos
  else model
 where
  repos          = getRepos model
  noPendingRepos = Map.null $ Map.filter isRepoComplete repos

  isRepoComplete (GitRepo _ _ _ (CleandRepo _ _)) = False
  isRepoComplete _ = True
