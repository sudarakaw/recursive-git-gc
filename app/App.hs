-- app/App.hs: Brick based main module
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module App
  ( app
  ) where


import           Brick.BChan                    ( writeBChan )
import           Brick.Main
import           Brick.Types                    ( BrickEvent(..)
                                                , EventM
                                                , Next
                                                )

import           Control.Concurrent             ( forkIO )
import           Control.Concurrent.MVar
import           Control.Monad.IO.Class         ( liftIO )

import           Data.Functor                   ( (<&>) )
import qualified Data.HashMap.Strict           as Map

import           Graphics.Vty                   ( Event(..)
                                                , Key(..)
                                                )

import qualified Fs
import           GitRepo                       as Repo
import           Model
import           View


-- UPDATE


viewportBody :: ViewportScroll ResourceName
viewportBody = viewportScroll ViewportBody

update
  :: Model -> BrickEvent ResourceName Msg -> EventM ResourceName (Next Model)
-- Handle user request to scroll (upwards) the repository list
update model (VtyEvent (EvKey KUp [])) =
  vScrollBy viewportBody (-1) >> continue model

-- Handle user request to scroll (downwards) the repository list
update model (VtyEvent (EvKey KDown [])) =
  vScrollBy viewportBody 1 >> continue model

-- Handle user request to quit the application
update completeModel@(Complete _ _) (VtyEvent (EvKey (KChar 'q') [])) =
  halt completeModel

-- Handle the event where new root directory is selected
update model (AppEvent (RootSelected dir)) = do
  -- Get Git repositories under given directory, and put them in `Map` as
  -- `GitRepo`keyed with the full path.
  repos <-
    liftIO
    $   Fs.find ".git" dir
    >>= mapM fromPath
    <&> map (\repo@(GitRepo path _ _ _) -> (path, repo))
    <&> Map.fromList


  -- For each Git repository found, spawn a thread to get the current disk usage
  -- of the content in `.git` directory.
  mapM_
    (\mapRepo -> do
      mvRepo <- liftIO $ newMVar mapRepo

      liftIO $ forkIO $ writeBChan (getChannel model) (RawRepoFound mvRepo)
    )
    repos

  -- While Git repository sizes are being calculated, store the raw repositories
  -- in the model so user see they are being processed.
  -- When there are no repositories found under the given root directory, this
  -- will switch the application state directory to `Complete`.
  continue . Model.isComplete . Model.withRepoMap repos $ model

-- Handle  the request to process raw repository
update model (AppEvent (RawRepoFound mvRepo)) = do
  -- Get the repository (`.git` directory) disk usage
  rawRepo@(GitRepo path _ _ _) <- liftIO $ takeMVar mvRepo
  size                         <- liftIO $ Fs.du path

  -- Update model with the sized repository information
  let repo = Repo.withSize size rawRepo

  _ <- liftIO $ forkIO $ writeBChan (getChannel model) (GotRepoSize repo)

  continue $ Model.withGitRepo repo model

-- Handle sized repository by running garbage collection on it
update model (AppEvent (GotRepoSize repo@(GitRepo path _ _ _))) = do
  _ <- liftIO $ Repo.gc path
  _ <- liftIO $ forkIO $ writeBChan (getChannel model) (GCComplete repo)

  continue model

-- Update model with garbage collection completed repositories
update model (AppEvent (GCComplete repo@(GitRepo path _ _ (SizedRepo sizeBefore))))
  = do
    sizeAfter <- liftIO $ Fs.du path

    continue $ Model.isComplete $ Model.withGitRepo
      (repo { status = CleandRepo sizeBefore sizeAfter })
      model

-- Fallback for all other events
update model _ = continue model


-- MAIN


app :: App Model Msg ResourceName
app = App { appDraw         = View.draw
          , appChooseCursor = showFirstCursor
          , appHandleEvent  = update
          , appStartEvent   = pure
          , appAttrMap      = View.styles
          }
