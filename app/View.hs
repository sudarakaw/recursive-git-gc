-- app/View.hs: UI rendering sub-routines for Brick
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module View
  ( draw
  , styles
  ) where


import           Brick.AttrMap
import           Brick.Types
import           Brick.Util
import           Brick.Widgets.Border
import           Brick.Widgets.Border.Style
import           Brick.Widgets.Core

import qualified Data.HashMap.Strict           as Map
import           Data.List                      ( sortBy )
import           Data.Maybe                     ( fromMaybe )

import           Graphics.Vty                  as V


import qualified Config
import           GitRepo                       as Repo
import           Model


-- STYLES


styles :: Model -> AttrMap
styles _ = attrMap
  mempty
  [ (attrName ""                , defAttr)
  , (attrName "body_text"       , fg white)
  , (attrName "border"          , withStyle (fg colorGitOrange) dim)
  , (attrName "branch"          , fg brightBlack)
  , (attrName "clean_icon"      , withStyle (fg green) (bold + dim))
  , (attrName "folder"          , withStyle (fg brightYellow) bold)
  , (attrName "footer"          , withStyle (fg white) dim)
  , (attrName "processing_icon" , fg brightBlack)
  , (attrName "logo"            , fg colorGitOrange)
  , (attrName "saved_space"     , fg green)
  , (attrName "saved_space_icon", withStyle (fg brightGreen) bold)
  , (attrName "title"           , withStyle (fg white) bold)
  , (attrName "version"         , withStyle (fg white) (bold + dim))
  , (attrName "white_bold"      , withStyle (fg white) bold)
  , (attrName "white_bright"    , fg brightWhite)
  ]


colorGitOrange :: Color
colorGitOrange = rgbColor 0xff 0x87 (0x00 :: Int)


-- VIEWS


draw :: Model -> [Widget ResourceName]
draw (Lookup  _ root ) = lookupView root
draw (Process _ repos) = mainView
  (locationListView repos)
  (str $ repoCountText repos ++ " found, cleaning in progress...")
draw (Complete _ repos) = mainView
  (locationListView repos)
  (   str (repoCountText repos ++ " garbage collected. ")
  <+> savedSpaceText
  <+> fill ' '
  <+> str "Press "
  <+> attr "white_bold" (str "q")
  <+> str " to exit."
  )
 where
  savedSpace =
    sum
      . map (uncurry (-) . tmap (fromMaybe 0) . Repo.getSize)
      . Map.elems
      $ repos
  savedSpaceText = if savedSpace > 0
    then
      (attr "saved_space_icon" . str $ show savedSpace)
        <+> (attr "saved_space" . str $ " disk space freed. ")
    else emptyWidget

locationListView :: RepoMap -> Widget ResourceName
locationListView =
  vBox . map locationView . Map.toList . Repo.byLocation . Map.elems

locationView :: (FilePath, [GitRepo]) -> Widget ResourceName
locationView (repoLoc, repoList) =
  attr "folder" (str "\x1f5c1  ")
    <+> attr "white_bold" (str repoLoc)
    <=> vBox (lmap repoView $ sortBy reposizeOder repoList)
    <=> vLimit 1 (fill ' ')

repoView :: Bool -> GitRepo -> Widget ResourceName
repoView isLast (GitRepo _ name _ currentStatus) =
  attr "branch" (str $ branchChar ++ "\x2500 ")
    <+> attr "folder" (str "\x1f5c0")
    <+> hBox [vLimit 1 $ hLimit 45 $ str ("  " ++ name) <+> fill ' ']
    <+> statusView currentStatus
  where branchChar = if isLast then "\x2514" else "\x251c"

statusView :: RepoStatus -> Widget ResourceName
statusView (SizedRepo size) =
  sizeColumnView (str . show $ size) (attr "processing_icon" . str $ "\xf085")
statusView (CleandRepo before after) = sizeColumnView (str . show $ before)
                                                      result
 where
  result = if before > after
    then attr "saved_space" (str (show (before - after)))
      <+> attr "saved_space_icon" (str " \x2193")
    else attr "clean_icon" (str " \xf00c")
statusView _ =
  sizeColumnView (str "") (attr "processing_icon" . str $ "\xf085")

sizeColumnView
  :: Widget ResourceName -> Widget ResourceName -> Widget ResourceName
sizeColumnView col1 col2 = hBox [vLimit 1 $ hLimit 9 $ fill ' ' <+> col1]
  <+> hBox [vLimit 1 $ hLimit 11 $ fill ' ' <+> col2]

lookupView :: FilePath -> [Widget ResourceName]
lookupView root = mainView
  emptyWidget
  (attr "white_bright" (str "\x1f5b4") <+> str (" " ++ root ++ "..."))

mainView :: Widget ResourceName -> Widget ResourceName -> [Widget ResourceName]
mainView body footer =
  [ joinBorders
        -- outer border
      $ withBorderStyle unicodeBold
      $ attr "border"
      $ border
      $ vBox
          [ -- header
            vLimit 1
          $   padLeftRight 1
          $   attr "logo"  (str "\xf841")
          <+> attr "title" (str " Git Repository Garbage Collector")
          <+> fill ' '
          <+> attr "version" (str Config.version)
          , hBorder
            -- content area
          , viewport ViewportBody Vertical
            $ vBox [padLeftRight 1 $ attr "body_text" body]
            -- footer
          , hBorder
          , vLimit 1 $ padLeftRight 1 $ attr "footer" footer
          ]
  ]


-- HELPERS

attr :: String -> Widget n -> Widget n
attr = withAttr . attrName

-- Simple implementation of `map` function over a generic List with a boolean
-- flag indicating if the element being processed is the last or not.
lmap :: (Bool -> a -> b) -> [a] -> [b]
lmap _ []       = []
lmap f [x     ] = [f True x]
lmap f (x : xs) = f False x : lmap f xs

-- simple implementation of `map` over a 2-Tuple
tmap :: (a -> b) -> (a, a) -> (b, b)
tmap f (x, y) = (f x, f y)

reposizeOder :: GitRepo -> GitRepo -> Ordering
reposizeOder repoA repoB = compare sizeB sizeA
 where
  sizeA = Repo.getSize repoA
  sizeB = Repo.getSize repoB

repoCountText :: RepoMap -> String
repoCountText repos = show count ++ " repositor" ++ if 1 == count
  then "y"
  else "ies"
  where count = length repos
