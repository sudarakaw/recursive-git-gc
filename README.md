# Recursive Git GC

Locate Git repositories (`.git` directories) recursively, and run the garbage
collect on them.

## License

Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
