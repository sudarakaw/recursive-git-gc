-- src/GitRepo.hs: sub-routines to scan for git repositories
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module GitRepo
  ( GitRepo(..)
  , RepoMap
  , RepoStatus(..)
  , byLocation
  , fromList
  , fromPath
  , gc
  , getSize
  , withSize
  ) where


import           Control.Exception              ( IOException
                                                , try
                                                )

import           Data.HashMap.Strict            ( HashMap )
import qualified Data.HashMap.Strict           as Map
import           Data.List                      ( foldl' )

import           System.Exit                    ( ExitCode )
import           System.FilePath                ( joinPath
                                                , splitDirectories
                                                )
import           System.Process                 ( CreateProcess(..)
                                                , readCreateProcessWithExitCode
                                                , shell
                                                )

import           Fs.Size                        ( Size )


-- MODEL


data RepoStatus = RawRepo
                | SizedRepo Size
                | CleandRepo { sizeBeforeClean :: Size
                             , sizeAfterClean :: Size
                             }
  deriving Show

data GitRepo = GitRepo
  { _path    :: FilePath
  , workdir  :: String
  , location :: FilePath
  , status   :: RepoStatus
  }
  deriving Show

type RepoMap = HashMap FilePath GitRepo


-- CREATORS


fromPath :: FilePath -> IO GitRepo
fromPath path = do
  let (_ : wd : rest) = reverse . splitDirectories $ path
      loc             = joinPath . reverse $ rest

  pure $ GitRepo path wd loc RawRepo

fromList :: [GitRepo] -> RepoMap
fromList rs = Map.fromList $ map (\r@(GitRepo path _ _ _) -> (path, r)) rs

withSize :: Size -> GitRepo -> GitRepo
withSize _ r@(GitRepo _ _ _ (SizedRepo _)) = r
withSize repoSize r = r { status = SizedRepo repoSize }


-- TRANSFORMERS


-- Return the repo list in the `Model` grouped by it's location
byLocation :: [GitRepo] -> HashMap FilePath [GitRepo]
byLocation = foldl' mapper Map.empty
 where
  mapper locMap repo =
    let loc = location repo
    in  if Map.member loc locMap
           -- location already in the map, prepend to the repository list
          then Map.adjust (repo :) loc locMap
           -- location not in the map, add new entry to the map with singleton
           -- list
          else Map.insert loc [repo] locMap


-- GETTERS


getSize :: GitRepo -> (Maybe Size, Maybe Size)
getSize (GitRepo _ _ _ RawRepo                  ) = (Nothing, Nothing)
getSize (GitRepo _ _ _ (SizedRepo usage        )) = (Just usage, Nothing)
getSize (GitRepo _ _ _ (CleandRepo before after)) = (Just before, Just after)


-- OPERATIONS


-- Run reflog clearing & garbage collection on Git repository at given path
gc :: FilePath -> IO (Either IOException (ExitCode, String, String))
gc path = try $ readCreateProcessWithExitCode
  (shell
      "git reflog expire --all --expire=all --expire-unreachable=all || git gc --aggressive --prune=now"
    )
    { cwd = Just path
    }
  ""
