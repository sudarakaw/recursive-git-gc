-- src/Fs.hs: file system based sub-routines.
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module Fs
  ( find
  , du
  ) where


import           System.Exit                    ( ExitCode(..) )
import           System.IO.Error                ( catchIOError )
import           System.Process                 ( shell
                                                , readCreateProcessWithExitCode
                                                )

import           Text.Printf                    ( printf )

import           Fs.Size                        ( Size
                                                , fromBytes
                                                )


-- Return the total disk usage of give directory and it's sub-directories in
-- bytes.
du :: FilePath -> IO Size
du path = do
  catchIOError
      (readCreateProcessWithExitCode
        (shell $ "du -sB 1 " ++ path ++ " | cut -f1")
        ""
      )
      (\_ -> pure (ExitSuccess, "", ""))
    >>= (\(_, stdout, _) -> pure . fromBytes . read $ stdout)


-- Find directories within the given root directory and matching the `name`
find :: String -> FilePath -> IO [FilePath]
find name root = do
  catchIOError
      (readCreateProcessWithExitCode
        (shell $ printf "find %s -type d -name \"%s\" 2>/dev/null" root name)
        ""
      )
      (\_ -> pure (ExitSuccess, "", ""))
    >>= (\(_, stdout, _) -> pure . lines $ stdout)
