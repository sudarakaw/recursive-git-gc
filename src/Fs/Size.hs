-- src/Fs/Size.hs: printable data structure for file/directory size
--
-- Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify it
-- under the terms of the BSD 2-clause License. See the LICENSE file for more
-- details.
--

module Fs.Size
  ( Size
  , toBytes
  , fromBytes
  ) where


import           Text.Printf


data Size
  = Bytes { toBytes :: Integer }
  | Kb    { toBytes :: Integer, __ :: Double }
  | Mb    { toBytes :: Integer, __ :: Double }
  deriving Ord

instance Show Size where
  show (Bytes size) = printf "%d B " size
  show (Kb _ size ) = printf "%.0f KB" size
  show (Mb _ size ) = printf "%.1f MB" size

instance Eq Size where
  aSize == bSize = toBytes aSize == toBytes bSize

instance Num Size where
  aSize + bSize =
    let a = toBytes aSize
        b = toBytes bSize
    in  fromBytes $ a + b

  aSize * bSize =
    let a = toBytes aSize
        b = toBytes bSize
    in  fromBytes $ a * b

  negate size = let s = toBytes size in fromBytes $ negate s

  abs size = let s = toBytes size in fromBytes $ abs s

  signum size = let s = toBytes size in fromBytes $ signum s

  fromInteger = fromBytes


-- Convert size to largest unit while maintaining the `Show` value < 0 and
-- preserving the original byte value
compactSize_ :: Size -> Size
compactSize_ (Bytes bytes) = if 1024 <= abs bytes
  then compactSize_ $ Kb bytes (fromInteger bytes / 1024.0)
  else Bytes bytes
compactSize_ (Kb bytes size) =
  if 1024 <= abs size then Mb bytes (size / 1024.0) else Kb bytes size
compactSize_ size = size


-- Convert given integer value to `Size` type
fromBytes :: Integer -> Size
fromBytes = compactSize_ . Bytes
